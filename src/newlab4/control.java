package newlab4;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import newlab4.Model;

public class control {

	

	public String formatone(String str) {
		return model.formatone(str);
	}

	public String formatetwo(String str) {
		return model.formatetwo(str);

	}

	public String formatethree(String str) {
		return model.formatethree(str);

	}

	public String formatefourth(String str) {
		return model.formatefourth(str);
	}

	public String formatefifth(String str) {
		return model.formatefifth(str);
	}

	public static void main(String[] args) {
		new control();
	}

	public control() {
		gui = new framebuilder();
		model = new Model();
		// gui.setSize(600,800);
		gui.frame.setBounds(500, 500, 562, 442);
		gui.frame.setTitle("Nested Loop ");
		gui.frame.setLocation(400, 150);
		gui.frame.setVisible(true);
		list = new ListenerMgr();
		gui.setListener(list);
		gui.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		

	}
	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			gui.wordcombo = (String) gui.comboBox.getSelectedItem();

			String str1 = gui.text1.getText();

			if (gui.wordcombo == "1") {

				gui.showresult.setText(model.formatone(str1));
			}
			if (gui.wordcombo == "2") {
				gui.showresult.setText(model.formatetwo(str1));
			}
			if (gui.wordcombo == "3") {
				gui.showresult.setText(model.formatethree(str1));
			}
			if (gui.wordcombo == "4") {
				gui.showresult.setText(model.formatefourth(str1));
			}
			if (gui.wordcombo == "5") {
				gui.showresult.setText(model.formatefifth(str1));
			}
			if (e.getSource() == gui.btnExit) {
				System.exit(0);
			}
			if (e.getSource() == gui.btnClear) {
				gui.text1.setText("");

			}

		}

	}
	ActionListener list;
	framebuilder gui;
	Model model;
}
