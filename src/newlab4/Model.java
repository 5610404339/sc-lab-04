package newlab4;

public class Model {

	public String formatone(String str) {
		String empty = " ";
		int value = Integer.parseInt(str);
		for (int i = 1; i <= value; i++) {
			empty += "\n";
			for (int j = 1; j <= value; j++) {
				empty += " *";

			}

		}
		return empty;
	}

	public String formatetwo(String str) {
		String empty = " ";
		int value = Integer.parseInt(str);
		for (int i = 1; i <= value; i++) {
			empty += "\n";
			for (int j = 1; j <= value; j++) {
				empty += " *";

			}

		}
		return empty;
	}

	public String formatethree(String str) {
		String empty = " ";

		int value = Integer.parseInt(str);
		for (int i = 1; i <= value; i++) {
			empty += "\n";
			for (int j = 1; j <= i; j++) {
				empty += " *";

			}

		}
		return empty;

	}

	public String formatefourth(String str) {
		String empty = " ";
		int value = Integer.parseInt(str);
		for (int i = 1; i <= value; i++) {
			empty += "\n";
			for (int j = 1; j <= value; j++) {
				if (j % 2 == 0) {
					empty += "*";
				} else {
					empty += "-";
				}
			}

		}
		return empty;
	}

	public String formatefifth(String str) {
		String empty = " ";
		int value = Integer.parseInt(str);
		for (int i = 1; i <= value; i++) {
			empty += "\n";
			for (int j = 1; j <= value; j++) {
				if (i % 2 == j % 2) {
					empty += "*";
				} else {
					empty += " ";
				}
			}

		}
		return empty;
	}
}
