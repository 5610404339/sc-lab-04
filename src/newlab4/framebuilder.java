package newlab4;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.jgoodies.forms.factories.DefaultComponentFactory;

public class framebuilder extends JFrame {
	JFrame frame;
	JPanel p1, p2;
	JTextField text1;
	JTextArea showresult;
	JComboBox<String> comboBox;
	JButton runbutton, btnExit, btnClear;
	JLabel label1, label2;
	String str;
	String wordcombo;

	public framebuilder() {
		setframe();
	}

	public void setframe() {
		frame = new JFrame();
		label1 = new JLabel("COLUM & ROW");

		runbutton = new JButton("RUN");
		btnExit = new JButton("EXIT");

		frame.setBounds(600, 500, 562, 442);
		label1.setFont(new Font("Tahoma", Font.BOLD, 11));
		label1.setBounds(10, 10, 103, 14);
		frame.add(label1);

		text1 = new JTextField();
		text1.setBounds(10, 35, 208, 41);
		frame.add(text1);

		String[] menu = { "", "1", "2", "3", "4", "5" };
		comboBox = new JComboBox(menu);
		comboBox.setBounds(36, 120, 154, 20);
		frame.add(comboBox);

		runbutton.setBounds(20, 160, 89, 23);
		frame.add(runbutton);

		btnExit.setBounds(119, 160, 89, 23);
		frame.add(btnExit);

		showresult = new JTextArea();
		showresult.setBounds(259, 30, 277, 363);
		showresult.setFont(new Font("Tahoma", Font.BOLD, 30));
		frame.add(showresult);
		showresult.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createEtchedBorder()));

		btnClear = new JButton("Clear");
		btnClear.setBounds(66, 82, 89, 23);
		frame.add(btnClear);

		label2 = DefaultComponentFactory.getInstance().createTitle(" ");
		label2.setBounds(259, 10, 88, 14);
		frame.add(label2);

	}

	public void setResult(String str) {
		this.str = str;
		showresult.setText(str);
	}

	public void setListener(ActionListener list) {
		runbutton.addActionListener(list);
		btnExit.addActionListener(list);
		btnClear.addActionListener(list);

	}
}
